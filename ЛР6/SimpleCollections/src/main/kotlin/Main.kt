import java.lang.System.`in`
import java.util.*
import java.lang.Math.abs
import kotlin.collections.HashSet

class Main() {
    // entry point
    fun main() {
        println("hi, Kotli");

        val drivingLicense: DrivingLicense = DrivingLicense("Петя", "Петров",
            Date(1999, 11, 23), Date(2015, 7, 2),
            "Петропавловск-Камчатский", 2222, 333333)
        val drivingLicense2: DrivingLicense = DrivingLicense("Витя", "Ветров",
            Date(2001, 10, 26), Date(2022, 1, 4),
            "Витебск", 3333, 444444)
        val drivingLicense3: DrivingLicense = DrivingLicense("Коля", "Котов",
            Date(2003, 2, 6), Date(2021, 9, 12),
            "Кострома", 3333, 444443)
        // drivingLicense.printDrivingLicense()

        var listDrivindLicenses: MutableList<DrivingLicense> = mutableListOf(
            drivingLicense, drivingLicense2, drivingLicense3
        )
        println("Список до сортировки:")
        listDrivindLicenses.forEach{it.printDrivingLicense()}
        listDrivindLicenses.sortWith(DrivingLicense.SeriesNumberComparator)
        println("Список после сортировки по серии-номеру:")
        listDrivindLicenses.forEach{it.printDrivingLicense()}
        listDrivindLicenses.sortWith(DrivingLicense.DateOfIssueComparator)
        println("Список после сортировки по дате выдачи:")
        listDrivindLicenses.forEach{it.printDrivingLicense()}
    }


    // task 3
    // Дано вещественное число R и массив вещественных чисел
    // Поиск элемента массива, который наиболее близок к данному числу
    fun getNearestItem(R: Double, list: MutableList<Double>): Double {
        var listDistances = list.map{Math.abs(it - R)}.toMutableList()
        return list[listDistances.indexOf(listDistances.min())]
    }

    // Поиск количества элементов между первым и последним минимальным
    fun getCountItemsBetweenFirstAndLastMin(list: MutableList<Int>): Int {
        val minItem = list.min()
        return list.lastIndexOf(minItem) - list.indexOf(minItem) - 1
    }

    // Дан целочисленный массив и интервал a..b. Необходимо найти количество
    // минимальных элементов в этом интервале
    // Полагаем, что есть несколько одинаковых минимальных элементов в массиве, и если они входят в интервал, то выдать их количество
    fun getCountMinItemsInInterval(list: MutableList<Int>, a:Int, b: Int): Int {
        val minItem = list.min()
        return if ((minItem > a)&&(minItem < b)) list.count { it == minItem } else 0
    }

    // найти элементы, расположенные между первым и вторым максимальным
    // предполагаем, что в списке два одинаковых максимальных элемента
    fun getItemsBetweenFirstSecondMax(list: MutableList<Int>): MutableList<Int> {
        val maxItem = list.max()
        val indFirstMax = list.indexOf(maxItem)
        var list2: MutableList<Int> = list.toMutableList()
        list2.removeAt(indFirstMax)
        val indSecondMax = list2.indexOf(maxItem)+1
        return list.filterIndexed { index, i -> (index > indFirstMax) && (index < indSecondMax) }.toMutableList()
    }

    // Найти элементы, расположенные перед последним минимальным
    fun getListItemsBeforeLastMin(list: MutableList<Int>): MutableList<Int> {
        val minItem = list.min()
        list.reverse()
        var newlist: MutableList<Int> = deleteItemsBeforeFirstMin(list, minItem)
        newlist.reverse()
        return newlist
    }
    // Удалить элементы перед минимум включительно
    fun deleteItemsBeforeFirstMin(list: MutableList<Int>, minItem: Int): MutableList<Int> {
        if (list[0] != minItem) {
            list.removeAt(0)
            return deleteItemsBeforeFirstMin(list, minItem)
        } else {
            list.removeAt(0)
            return list
        }
    }

    // Вывести индексы массива в том порядке, в котором соответствующие
    // им элементы образуют убывающую последовательность
    fun getIndexesDescending(list: MutableList<Int>): Unit {
        var newmap: MutableMap<Int, Int> = list.mapIndexed{ index, i -> index to i  }.toMap().toMutableMap()
        // newmap.forEach{ println("${it.key} -> ${it.value}  ") }
        println()
        list.sortDescending()
        printIndexFromMap(list, newmap)
    }
    fun printIndexFromMap(list: MutableList<Int>, map: MutableMap<Int, Int>): Unit {
        if (list.isNotEmpty()) {
            var copymap = map.toMutableMap()
            var setKeys = copymap.filterValues { i -> i == list.get(0) }.keys
            println(setKeys.first())
            map.remove(setKeys.first())
            list.removeAt(0)
            printIndexFromMap(list, map)
        } else {
            println("Конец")
        }
    }

    // task 2
    // составляет список, состоящий из кортежей длины 3, где каждый кортеж (ai,bi,ci) получен следующим образом:
    // ai – i по убыванию элемент первого списка
    // bi – i по возрастанию суммы цифр элемент второго списка
    // ci - i по убыванию количества делителей элемент третьего списка
    fun hardList(a: MutableList<Int>, b: MutableList<Int>, c: MutableList<Int>): MutableList<MutableList<Int>> {
        if ((a.isNotEmpty())&&(b.isNotEmpty())&&(c.isNotEmpty())&&
            (a.size==b.size)&&(b.size==c.size)) {
            a.sortDescending()
            b.sort()
            b.sortBy { sumc(it) }
            c.sortDescending()
            c.sortByDescending { countDels(it) }
            var newList: MutableList<MutableList<Int>> = mutableListOf()
            fun insteadOfForEach(): Unit {
                if (a.isNotEmpty()) {
                    newList.add(mutableListOf(a[0], b[0], c[0]))
                    a.removeAt(0)
                    b.removeAt(0)
                    c.removeAt(0)
                    insteadOfForEach()
                }
            }
            insteadOfForEach()
            // a.forEachIndexed { index, i -> newList.add(mutableListOf(i, b[index], c[index])) }
            // newList.forEach{println(it)}
            return newList
        } else {
            return mutableListOf()
        }
    }
    // Подсчитывает количество делителей
    fun countDels(n: Int): Int = countDels2(n, n)
    fun countDels2(n: Int, del: Int): Int =
        if (del == 1) 1
        else
            countDels2(n, del-1) + if (n%del==0) 1 else 0

    // указывает, сколько элементов из списка могут быть квадратом какого-то из элементов списка.
    fun countSquareNumbers(mList: MutableList<Int>): Int? {
        if (mList.isNotEmpty()) {
            val squaresList = mList.toMutableList().map { it * it }
            return mList.count { it in squaresList }
        } else {
            return null
        }
    }

    // task 1
    fun main2() {
        // println(newlist(mutableListOf(235, 23, -23,12, -535, 352, 32, 54, 46, -436, -3, 43)))
        // println(new2freq(mutableListOf(32, 23, 43, 534, 12, 23, 3, 43, 23, 32, 56)))
        // println(freq(mutableListOf(34, 32, 35, 32, 325)))
        // println(maxd(vvod(10)))
        // println(sumd(vvod(10)))
        // println(muld(vvod(4)))
        // println(mind(vvod(10)))

        val m: MutableList<User> = mutableListOf()
        val u1=User("Фам","Им","От","@a",Date(1999,7,2))
        val u2=User("Иванов","Иван","Иванович","@ivan",Date(1964,2,12))
        val u3=User("Василий","Вася","","@lesnik",Date(1988,10,30))
        val u4=User("Иванов","Иван","Иванович","@ivan",Date(1964,2,12))
        m.add(u1)
        m.add(u2)
        m.add(u3)
        m.add(u4)
        for (x in m)x.write()
        println("-------------------------------------------------------------------------------")
        m.sort()
        for (x in m)x.write()
        println("-------------------------------------------------------------------------------")
        val h: HashSet<User> = hashSetOf()
        h.add(u1)
        h.add(u2)
        h.add(u3)
        h.add(u4)
        for (x in h)x.write()
    }
    // Удалить отрицательные числа, сумма цифр которых меньше 10
    fun newlist(m: MutableList<Int>): MutableList<Int>
    {
        if (m.isNotEmpty()) {
            val n:MutableList<Int> = mutableListOf()
            for (x in m) if ((x < 0) and (Main().sumc(abs(x)) < 10)) n.add(x)
            val listWithDeletedItems: MutableList<Int> = m.toMutableList()
            listWithDeletedItems.removeAll(n)
            return listWithDeletedItems
        } else return mutableListOf()
    }
    //сумма цифр вверх
    fun sumc(n: Int): Int = if (n < 10) n else (n % 10) + sumc(n / 10)
    // Получить только те чётные элементы, которые встречаются чётное число раз
    fun new2freq(m: MutableList<Int>): MutableList<Int>
    {
        if (m.isNotEmpty()) {
            val n:MutableList<Int> = mutableListOf()
            val map: MutableMap<Int, Int> = mutableMapOf()
            for (x in m) map[x] = map.getOrDefault(x, 0) + 1
            for (x in map) if ((x.key % 2 == 0) and (x.value % 2 == 0)) n.add(x.key)
            return n
        } else return mutableListOf()
    }
    // Находит для заданного списка моду
    fun freq(m: MutableList<Int>): Int
    {
        if (m.isNotEmpty()) {
            val map: MutableMap<Int, Int> = mutableMapOf()
            for (x in m) map[x] = map.getOrDefault(x, 0) + 1
            var k = 1
            var e: Int = map.values.first()
            for (x in map) if (x.value > k) {
                k = x.value
                e = x.key
            }
            return e
        } else return 0
    }

    val scanner = Scanner(`in`)
    fun vvodd(n: Int, m: MutableList<Int>) {
        if (n>0) {
            m.add(scanner.nextInt())
            vvodd(n-1, m)
        }
    }
    // Считать с клавиатуры n целых чисел
    fun vvod(n: Int):MutableList<Int> {
        val m:MutableList<Int> = mutableListOf()
        vvodd(n, m)
        return m
    }
    tailrec fun arrayOP(m: MutableList<Int>, i: Int, a: Int = 0, f: (Int, Int) -> Int): Int =
        if (i <= 0) a else arrayOP(m,i-1, f(a, m[i-1]), f)
    fun sumd(m: MutableList<Int>): Int = arrayOP(m, m.size, 0) { a, b -> (a + b) }
    fun muld(m: MutableList<Int>): Int = arrayOP(m, m.size, 1) { a, b -> (a * b) }
    fun maxd(m: MutableList<Int>): Int = arrayOP(m, m.size, m[0]) { a, b -> if (a > b) a else b }
    fun mind(m: MutableList<Int>): Int = arrayOP(m, m.size, m[0]) { a, b -> if (a < b) a else b }
}

fun main() = Main().main();