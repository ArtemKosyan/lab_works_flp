import java.util.Date

class DrivingLicense (val name: String, val surname: String, val birthday: Date, val dateOfIssue: Date,
                      val placeOfResidence: String, val series: Int, val number: Int) : Comparable<DrivingLicense>
{
    // Естественная сортировка по имени владельца прав
    override fun compareTo(other: DrivingLicense): Int = this.name.compareTo(other.name)
    init {
        validateName()
        validateSurname()
        validateBirthday()
        validateDateOfIssue()
        validatePlaceOfResidence()
        validateSeries()
        validateNumber()
    }

    fun validateName() {
        if (!(name.toString() matches Regex("^[А-Я,Ё][а-я,ё]+\$"))) {
            throw IllegalArgumentException("Некорректное имя")
        }
    }
    fun validateSurname() {
        if (!(surname.toString() matches Regex("^[А-Я,Ё][а-я,ё]+\$"))) {
            throw IllegalArgumentException("Некорректная фамилия")
        }
    }
    fun validateBirthday() {
        if (!(birthday.year >= 1900)) {
            throw IllegalArgumentException("Некорректная дата рождения: год должен быть после 1900")
        }
    }
    fun validateDateOfIssue() {
        if (!(dateOfIssue.year >= 1950)) {
            throw IllegalArgumentException("Некорректная дата выдачи: год должен быть после 1950")
        }
    }
    fun validatePlaceOfResidence() {
        if (!(placeOfResidence matches Regex("^[А-ЯЁ]([а-яё]+-[А-ЯЁ])?[а-яё]+\$"))) {
            throw IllegalArgumentException("Некорректное название места жительства")
        }
    }
    fun validateSeries() {
        if (!(series.toString() matches Regex("^[0-9]{4}$"))) {
            throw IllegalArgumentException("Некорректная серия документа: должно быть четыре цифры")
        }
    }
    fun validateNumber() {
        if (!(number.toString() matches Regex("^[0-9]{6}$"))) {
            throw IllegalArgumentException("Некорректный номер документа: должно быть шесть цифр")
        }
    }

    fun printDrivingLicense() {
        println(
            "Права на вождение\n" +
                    "\tИмя : $name\n" +
                    "\tФамилия : $surname\n" +
                    "\tМесто жительства : $placeOfResidence\n" +
                    "\tСерия : $series\n" +
                    "\tНомер : $number\n" +
                    "\tДата рождения : ${birthday.day}.${birthday.month}.${birthday.year}\n" +
                    "\tДата выдачи : ${dateOfIssue.day}.${dateOfIssue.month}.${dateOfIssue.year}\n"
        )
    }
    // Проверка равенства документов по серии, номеру
    override fun equals(other: Any?): Boolean =
        if (other is DrivingLicense) ((this.series==other.series)&&(this.number==other.number))
        else false

    override fun hashCode(): Int {
        var result = series.hashCode()
        result = 31 * result + number.hashCode()
        return result
    }

    // Для сравнений по паре серия-номер
    object SeriesNumberComparator: Comparator<DrivingLicense> {
        override fun compare(o1: DrivingLicense, o2: DrivingLicense): Int =
            when {
                o1.series > o2.series -> 1
                o1.series < o2.series -> -1
                o1.number > o2.number -> 1
                o1.number < o2.number -> -1
                else -> 0
            }
    }
    // Для сравнений по дате выдачи
    object DateOfIssueComparator: Comparator<DrivingLicense> {
        override fun compare(o1: DrivingLicense, o2: DrivingLicense): Int =
            o1.dateOfIssue.compareTo(o2.dateOfIssue)
    }
}