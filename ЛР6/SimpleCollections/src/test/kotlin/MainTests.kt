import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.collections.HashSet


internal class DrivingLicenseTests {
    val drivingLicense: DrivingLicense = DrivingLicense("Петя", "Петров",
        Date(1999, 11, 23), Date(2015, 7, 2),
        "Петропавловск-Камчатский", 2222, 333333)
    val drivingLicense2: DrivingLicense = DrivingLicense("Витя", "Ветров",
        Date(2001, 10, 26), Date(2022, 1, 4),
        "Витебск", 3333, 444444)
    val drivingLicense3: DrivingLicense = DrivingLicense("Коля", "Котов",
        Date(2003, 2, 6), Date(2021, 9, 12),
        "Кострома", 3333, 444443)
    @Test
    fun treeSetTests() {
        var treeSet: TreeSet<DrivingLicense> = TreeSet<DrivingLicense>()
        treeSet.add(drivingLicense)
        treeSet.add(drivingLicense2)
        treeSet.add(drivingLicense3)
        assertEquals(true, treeSet.contains(DrivingLicense("Петя", "Петров",
            Date(1999, 11, 23), Date(2015, 7, 2),
            "Петропавловск-Камчатский", 2222, 333333)))
        assertEquals(true, treeSet.contains(DrivingLicense("Витя", "Ветров",
            Date(2001, 10, 26), Date(2022, 1, 4),
            "Витебск", 3333, 444444)))
        assertEquals(true, treeSet.contains(DrivingLicense("Коля", "Котов",
            Date(2003, 2, 6), Date(2021, 9, 12),
            "Кострома", 3333, 444443)))
    }
    @Test
    fun hashSetTests() {
        val hashSet: HashSet<DrivingLicense> = hashSetOf(
            drivingLicense, drivingLicense2, drivingLicense3
        )
        assertEquals(true, hashSet.contains(DrivingLicense("Петя", "Петров",
            Date(1999, 11, 23), Date(2015, 7, 2),
            "Петропавловск-Камчатский", 2222, 333333)))
        assertEquals(true, hashSet.contains(DrivingLicense("Витя", "Ветров",
            Date(2001, 10, 26), Date(2022, 1, 4),
            "Витебск", 3333, 444444)))
        assertEquals(true, hashSet.contains(DrivingLicense("Коля", "Котов",
            Date(2003, 2, 6), Date(2021, 9, 12),
            "Кострома", 3333, 444443)))
    }
}

internal class MainTests {

    @Test
    fun getNearestItemTest2() {
        val main = Main()
        val expected: Double = 2.2213
        assertEquals(expected, main.getNearestItem(2.222, mutableListOf(2.223, 2.221, 2.2212, 2.2232, 2.2213, 2.2230)))
    }
    @Test
    fun getNearestItemTest1() {
        val main = Main()
        val expected: Double = 5.00001
        assertEquals(expected, main.getNearestItem(5.0, mutableListOf(4.998, 4.999, 5.00001, 5.001, 5.01)))
    }
    @Test
    fun getCountItemsBetweenFirstAndLastMinTest3() {
        val main = Main()
        val expected: Int = 0
        assertEquals(expected, main.getCountItemsBetweenFirstAndLastMin(mutableListOf(500, 500)))
    }
    @Test
    fun getCountItemsBetweenFirstAndLastMinTest2() {
        val main = Main()
        val expected: Int = 4
        assertEquals(expected, main.getCountItemsBetweenFirstAndLastMin(mutableListOf(1, 23, 23, 32, 32, 1)))
    }
    @Test
    fun getCountItemsBetweenFirstAndLastMinTest1() {
        val main = Main()
        val expected: Int = 12
        assertEquals(expected, main.getCountItemsBetweenFirstAndLastMin(mutableListOf(8, 6, 21, 43, 8, 6, 23, 91, 12, 23, 6, 27, 32,12, 6)))
    }
    @Test
    fun getCountMinItemsInIntervalTest3() {
        val main = Main()
        val expected: Int = 4
        assertEquals(expected, main.getCountMinItemsInInterval(mutableListOf(8, 6, 21, 43, 8, 6, 23, 91, 12, 23, 6, 27, 32,12, 6), 4, 8))
    }
    @Test
    fun getCountMinItemsInIntervalTest2() {
        val main = Main()
        val expected: Int = 0
        assertEquals(expected, main.getCountMinItemsInInterval(mutableListOf(7, 2, 3, 4, 5, 4, 3, 2, 7), 1, 2))
    }
    @Test
    fun getCountMinItemsInIntervalTest1() {
        val main = Main()
        val expected: Int = 2
        assertEquals(expected, main.getCountMinItemsInInterval(mutableListOf(7, 2, 3, 4, 5, 4, 3, 2, 7), 1, 4))
    }
    @Test
    fun getItemsBetweenFirstSecondMaxTest3() {
        val main = Main()
        val expected: MutableList<Int> = mutableListOf(2, 3, 4, 5, 4, 3, 2)
        assertEquals(expected, main.getItemsBetweenFirstSecondMax(mutableListOf(7, 2, 3, 4, 5, 4, 3, 2, 7)))
    }
    @Test
    fun getItemsBetweenFirstSecondMaxTest2() {
        val main = Main()
        val expected: MutableList<Int> = mutableListOf(24, 54, 21, 43, 21, 33)
        assertEquals(expected, main.getItemsBetweenFirstSecondMax(mutableListOf(33, 44, 22, 123, 24, 54, 21, 43, 21, 33, 123, 34, 54, 22, 33, 123, 43, 54)))
    }
    @Test
    fun getItemsBetweenFirstSecondMaxTest1() {
        val main = Main()
        val expected: MutableList<Int> = mutableListOf(2, 4, 3)
        assertEquals(expected, main.getItemsBetweenFirstSecondMax(mutableListOf(5, 2, 4, 3, 5, 1, 5, 2, 4, 3, 5, 1, 3, 4, 5)))
    }
    @Test
    fun getListItemsBeforeLastMinTest2() {
        val main = Main()
        val expected: MutableList<Int> = mutableListOf(5, 2, 4, 3, 5, 1, 5, 2, 4, 3, 5)
        assertEquals(expected, main.getListItemsBeforeLastMin(mutableListOf(5, 2, 4, 3, 5, 1, 5, 2, 4, 3, 5, 1, 3, 4, 5, 6)))
    }
    @Test
    fun getListItemsBeforeLastMinTest1() {
        val main = Main()
        val expected: MutableList<Int> = mutableListOf(5, 2, 4, 3, 5)
        assertEquals(expected, main.getListItemsBeforeLastMin(mutableListOf(5, 2, 4, 3, 5, 1, 3, 4, 5, 6)))
    }
    @Test
    fun deleteItemsBeforeFirstMinTest2() {
        val main = Main()
        val expected: MutableList<Int> = mutableListOf(3, 4, 5, 6)
        assertEquals(expected, main.deleteItemsBeforeFirstMin(mutableListOf(5, 2, 4, 3, 5, 1, 3, 4, 5, 6), 1))
    }
    @Test
    fun deleteItemsBeforeFirstMinTest1() {
        val main = Main()
        val expected: MutableList<Int> = mutableListOf(4, 3, 5)
        assertEquals(expected, main.deleteItemsBeforeFirstMin(mutableListOf(5, 2, 4, 3, 5), 2))
    }
    @Test
    fun hardListTest2() {
        val main = Main()
        val expected: MutableList<MutableList<Int>> = mutableListOf(
            mutableListOf(5, 12, 243),
            mutableListOf(4, 33, 32),
            mutableListOf(3, 71, 16),
            mutableListOf(2, 45, 4),
            mutableListOf(1, 81, 5)
        )
        assertEquals(expected, main.hardList(
            mutableListOf(1, 2, 3, 4, 5),
            mutableListOf(33, 45, 81, 12, 71),
            mutableListOf(16, 32, 4, 5, 243)))
    }
    @Test
    fun hardListTest1() {
        val main = Main()
        val expected: MutableList<MutableList<Int>> = mutableListOf()
        assertEquals(expected, main.hardList(mutableListOf(), mutableListOf(), mutableListOf()))
    }
    @Test
    fun countDelsTest4() {
        val main = Main()
        val expected: Int = 2
        assertEquals(expected, main.countDels(2))
    }
    @Test
    fun countDelsTest3() {
        val main = Main()
        val expected: Int = 1
        assertEquals(expected, main.countDels(1))
    }
    @Test
    fun countDelsTest2() {
        val main = Main()
        val expected: Int = 5
        assertEquals(expected, main.countDels(16))
    }
    @Test
    fun countDelsTest1() {
        val main = Main()
        val expected: Int = 3
        assertEquals(expected, main.countDels(9))
    }
    @Test
    fun countSquareNumbersTest4() {
        val main = Main()
        val expected: Int? = 4
        assertEquals(expected, main.countSquareNumbers(mutableListOf(2, 4, 16, 256, 65536)))
    }
    @Test
    fun countSquareNumbersTest3() {
        val main = Main()
        val expected: Int? = 0
        assertEquals(expected, main.countSquareNumbers(mutableListOf(6, 7, 8, 9)))
    }
    @Test
    fun countSquareNumbersTest2() {
        val main = Main()
        val expected: Int? = 3
        // Девятка - квадрат тройки, четыре - квадрат двойки, единица - квадрат самой себя
        assertEquals(expected, main.countSquareNumbers(mutableListOf(1, 2, 3, 4, 5, 6, 7, 8, 9)))
    }
    @Test
    fun countSquareNumbersTest1() {
        val main = Main()
        val expected: Int? = null
        assertEquals(expected, main.countSquareNumbers(mutableListOf()))
    }
}