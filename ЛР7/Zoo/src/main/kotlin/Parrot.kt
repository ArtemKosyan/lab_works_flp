data class Parrot(val breed: String, val color: String,
    override var height: Int,
    override var weight: Int,
    override val name: String) : Animal(height, weight, name) {
}