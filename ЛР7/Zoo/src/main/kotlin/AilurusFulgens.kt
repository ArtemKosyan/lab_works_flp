data class AilurusFulgens(val birthplace: String,
                          override var height: Int,
                          override var weight: Int,
                          override val name: String) : Animal(height, weight, name) {
}