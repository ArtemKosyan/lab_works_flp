abstract class Animal(open var height: Int,
                      open var weight: Int,
                      open val name: String) {
}