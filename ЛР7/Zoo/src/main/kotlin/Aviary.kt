data class Aviary(val length: Int, val width: Int, val height: Int,
             val hasRoof: Boolean, var humidity: Int,
             val capacity: Int, val animalKind: String,
             val animals: MutableList<Animal>) {

}