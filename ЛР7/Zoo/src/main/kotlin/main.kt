class Main() {
    // entry point
    fun main() {
        println("Hi, Kotlin-Zoo")

        val popugai: Parrot = Parrot("Ara", "Red", 70, 30, "redara")
        println(popugai.toString())
        val redPanda: AilurusFulgens = AilurusFulgens("mountains os Asia", 50, 40, "redapanda")
        println(redPanda.toString())
        val aviary: Aviary = Aviary(1000, 1000, 1000, true, 40, 10,
            "Parrot", mutableListOf(
                Parrot("Ara", "Red", 70, 30, "redara"),
                Parrot("Ara", "Blue", 65, 27, "blueara")
            ))
        println(aviary.toString())
        
    }
}

fun main() = Main().main();