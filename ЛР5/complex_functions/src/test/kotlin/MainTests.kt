import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

// 7
internal class complexRecursionsTests {
    @Test
    fun hardNod1() {
        val main = Main();
        val expected = 1;
        assertEquals(expected, main.hardNod(129487))
    }
    @Test
    fun hardNod2() {
        val main = Main();
        val expected = 1071;
        assertEquals(expected, main.hardNod(1071))
    }
    @Test
    fun hardNod3() {
        val main = Main();
        val expected = 9;
        assertEquals(expected, main.hardNod(9))
    }

    @Test
    fun getMaxNotOddNotPrimeDel1() {
        val main = Main();
        val expected = 129487;
        assertEquals(expected, main.getMaxNotOddNotPrimeDel(129487))
    }
    @Test
    fun getMaxNotOddNotPrimeDel2() {
        val main = Main();
        val expected = 333;
        assertEquals(expected, main.getMaxNotOddNotPrimeDel(333))
    }
    @Test
    fun getMaxNotOddNotPrimeDel3() {
        val main = Main();
        val expected = 1;
        assertEquals(expected, main.getMaxNotOddNotPrimeDel(8768))
    }
    @Test
    fun proizvCifrUp1() {
        val main = Main();
        val expected = 720;
        assertEquals(expected, main.proizvCifrUp(32654))
    }
    @Test
    fun proizvCifrUp2() {
        val main = Main();
        val expected = 4032;
        assertEquals(expected, main.proizvCifrUp(129487))
    }
    @Test
    fun proizvCifrUp3() {
        val main = Main();
        val expected = 0;
        assertEquals(expected, main.proizvCifrUp(999880288))
    }
    @Test
    fun getMaxPrimeDel1() {
        val main = Main();
        val expected = 1834717;
        assertEquals(expected, main.getMaxPrimeDel(1834717))
    }
    @Test
    fun getMaxPrimeDel2() {
        val main = Main();
        val expected = 3;
        assertEquals(expected, main.getMaxPrimeDel(72))
    }
    @Test
    fun prostoe1() {
        val main = Main();
        val expected = true;
        assertEquals(expected, main.prostoe(1834717))
    }
    @Test
    fun prostoe2() {
        val main = Main();
        val expected = true;
        assertEquals(expected, main.prostoe(17))
    }
    @Test
    fun prostoe3() {
        val main = Main();
        val expected = false;
        assertEquals(expected, main.prostoe(1))
    }
    @Test
    fun prostoe4() {
        val main = Main();
        val expected = true;
        assertEquals(expected, main.prostoe(2))
    }
}

// 5
internal class NodTests {
    @Test
    fun nodLoop1() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.nodLoop(1, 1))
    }
    @Test
    fun nodLoop2() {
        val main = Main()
        val expected = 11111
        assertEquals(expected, main.nodLoop(22222, 33333))
    }
    @Test
    fun nodLoop3() {
        val main = Main()
        val expected = 26
        assertEquals(expected, main.nodLoop(2321618, 36322))
    }
    @Test
    fun nodUp1() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.nodUp(1, 1))
    }
    @Test
    // test failed. Выделил 4 гб, но спустилась с 22222 лишь до 4906
    fun nodUp2() {
        val main = Main()
        val expected = 11111
        assertEquals(expected, main.nodUp(22222, 33333))
    }
    @Test
    // test failed. ну тут без лишних слов...
    fun nodUp3() {
        val main = Main()
        val expected = 26
        assertEquals(expected, main.nodUp(2321618, 36322))
    }
    @Test
    // замутим значит такой тестик на ожидание ошибки переполнения
    fun nodUp4() {
        val main = Main()
        val expected = 26
        assertThrows(StackOverflowError::class.java) {
            assertEquals(expected, main.nodUp(2321618, 36322))
        }
    }
    @Test
    fun nodDown1() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.nodDown(1, 1))
    }
    @Test
    fun nodDown2() {
        val main = Main()
        val expected = 11111
        assertEquals(expected, main.nodDown(22222, 33333))
    }
    @Test
    fun nodDown3() {
        val main = Main()
        val expected = 26
        assertEquals(expected, main.nodDown(2321618, 36322))
    }
}
internal class ProizvCifrNotDelOnFiveTests {
    // tests for proizvCifrNotDelOnFive functions
    @Test
    fun proizvCifrNotDelOnFiveLoop1() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveLoop(555555))
    }
    @Test
    fun proizvCifrNotDelOnFiveLoop2() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveLoop(111))
    }
    @Test
    fun proizvCifrNotDelOnFiveLoop3() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveLoop(0))
    }
    @Test
    fun proizvCifrNotDelOnFiveLoop4() {
        val main = Main()
        val expected = 531441
        assertEquals(expected, main.proizvCifrNotDelOnFiveLoop(9990999))
    }
    @Test
    fun proizvCifrNotDelOnFiveUp1() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveUp(555555))
    }
    @Test
    fun proizvCifrNotDelOnFiveUp2() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveUp(111))
    }
    @Test
    fun proizvCifrNotDelOnFiveUp3() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveUp(0))
    }
    @Test
    fun proizvCifrNotDelOnFiveUp4() {
        val main = Main()
        val expected = 531441
        assertEquals(expected, main.proizvCifrNotDelOnFiveUp(9990999))
    }
    @Test
    fun proizvCifrNotDelOnFiveDown1() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveDown(555555))
    }
    @Test
    fun proizvCifrNotDelOnFiveDown2() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveDown(111))
    }
    @Test
    fun proizvCifrNotDelOnFiveDown3() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.proizvCifrNotDelOnFiveDown(0))
    }
    @Test
    fun proizvCifrNotDelOnFiveDown4() {
        val main = Main()
        val expected = 531441
        assertEquals(expected, main.proizvCifrNotDelOnFiveDown(9990999))
    }
}
internal class MinCifrTests {
    // tests for minCifr functions
    @Test
    fun minCifrLoop1() {
        val main = Main()
        val expected = 0
        assertEquals(expected, main.minCifrLoop(0))
    }
    @Test
    fun minCifrLoop2() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.minCifrLoop(981721276))
    }
    @Test
    fun minCifrLoop3() {
        val main = Main()
        val expected = 9
        assertEquals(expected, main.minCifrLoop(999999))
    }
    fun minCifrUp1() {
        val main = Main()
        val expected = 0
        assertEquals(expected, main.minCifrUp(0))
    }
    @Test
    fun minCifrUp2() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.minCifrUp(981721276))
    }
    @Test
    fun minCifrUp3() {
        val main = Main()
        val expected = 9
        assertEquals(expected, main.minCifrUp(999999))
    }
    fun minCifrDown1() {
        val main = Main()
        val expected = 0
        assertEquals(expected, main.minCifrDown(0))
    }
    @Test
    fun minCifrDown2() {
        val main = Main()
        val expected = 1
        assertEquals(expected, main.minCifrDown(981721276))
    }
    @Test
    fun minCifrDown3() {
        val main = Main()
        val expected = 9
        assertEquals(expected, main.minCifrDown(999999))
    }
}

internal class MainTests {
    // from 3
    @Test
    fun nodUp() {
        val main = Main()
        val expected = 20
        assertEquals(expected, main.nodUp(1260, 122420))
    }
    @Test
    fun nodDown() {
        val main = Main()
        val expected = 20
        assertEquals(expected, main.nodDown(1260, 122420))
    }
    @Test
    fun proizvCifrNotDelOnFiveDown() {
        val main = Main()
        val expected = 56
        assertEquals(expected, main.proizvCifrNotDelOnFiveDown(542075))
    }
    @Test
    fun proizvCifrNotDelOnFiveUp() {
        val main = Main()
        val expected = 56
        assertEquals(expected, main.proizvCifrNotDelOnFiveUp(542075))
    }
    @Test
    fun minCifrDown() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.minCifrUp(874328))
    }
    @Test
    fun minCifrUp() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.minCifrUp(874328))
    }

    // from 2
    @Test
    fun nodLoop() {
        val main = Main()
        val expected = 20
        assertEquals(expected, main.nodLoop(1260, 122420))
    }
    @Test
    fun proizvCifrNotDelOnFiveLoop() {
        val main = Main()
        val expected = 56
        assertEquals(expected, main.proizvCifrNotDelOnFiveLoop(542075))
    }
    @Test
    fun minCifrLoop() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.minCifrLoop(874328))
    }
}