import java.lang.System.`in`
import java.util.Scanner
import java.io.File

class Main() {
    fun main() {
        println("hi, Kotlin");
        val scanner = Scanner(`in`);
        var myName = scanner.nextLine();
        val func: ((Int)->Int)? = getFuncByName(myName)
        println(if (func!=null) func(234) else "было некорректное имя")
    }
    // 7
    // Найти НОД максимального нечётного непростого делителя числа и прозведения цифр данного числа
    fun hardNod(n: Int) = nodDown(getMaxNotOddNotPrimeDel(n), proizvCifrUp(n))
    fun getMaxNotOddNotPrimeDel(n: Int) = getMaxNotOddNotPrimeDel2(n, n);
    tailrec fun getMaxNotOddNotPrimeDel2(n: Int, divisor: Int): Int =
        if (divisor < 1) 0
        else
            if (((n%divisor) == 0)&&(!prostoe(divisor))&&(divisor%2==1)) divisor;
            else getMaxNotOddNotPrimeDel2(n, divisor-1)
    // произведение цифр цисла
    fun proizvCifrUp(n: Int): Int =
        if (n < 10) n else (n%10) * proizvCifrUp(n/10)

    // Найти максимальный простой делитель числа
    fun getMaxPrimeDel(n: Int) = getMaxPrimeDel2(n, n)
    tailrec fun getMaxPrimeDel2(n: Int, divisor: Int): Int =
        if (divisor < 1) 0 else if ((n % divisor == 0)&&(prostoe(divisor))) divisor else getMaxPrimeDel2(n, divisor-1);
    // узнать простое ли число
    fun prostoe(n: Int) = prostoe2(n, n-1, 1)
    tailrec fun prostoe2(n: Int, divisor: Int, countDels: Int): Boolean =
        if (countDels > 2) false
        else if (divisor < 1) if (countDels == 2) true else false
             else prostoe2(n, divisor-1, countDels + if (n%divisor==0) 1 else 0)

    // 6
    fun main2()
    {
        println("Укажите полный путь к файлу");
        val scanner = Scanner(`in`);
        val pathToFile = scanner.nextLine(); // F:\FFF\swipl\git_repo\ЛР5\complex_functions\input.txt
        val file = File(pathToFile);
        try {
            var lines = file.readLines()
            // println(lines);
            if (lines.size != 0)
            {
                val pathToOutputFile = pathToFile.substring(0, pathToFile.lastIndexOf('\\')+1)+"output.txt";
                // println(pathToOutputFile);
                val outFile = File(pathToOutputFile);
                outFile.writeText("")
                lines.forEach() {
                    try {
                        // println(it)
                        var numberAndFuncName = it.split(" ")
                        // println(numberAndFuncName)
                        val func: ((Int) -> Int)? = getFuncByName(numberAndFuncName[1])
                        // println(if (func!=null) func(numberAndFuncName[0].toInt()) else "было некорректное имя")
                        outFile.appendText(it + " " + (if (func != null) (func(numberAndFuncName[0].toInt())).toString() else "Было некорректное название функции") + "\n");
                    }
                    catch (e: Exception)
                    {
                        outFile.appendText("Некорректная структура строки\n");
                    }
                }
            }
            else
            {
                println("Пустой файл")
            }
        }
        catch (e: java.io.FileNotFoundException)
        {
            println("Не удалось найти указанный файл")
        }
    }

    fun getFuncByName(name: String): ((Int)->Int)? {
        val mapsFuncs:  Map<String, (Int)->Int > = mapOf(
            "proizvCifrNotDelOnFive" to ::proizvCifrNotDelOnFiveUp,
            "minCifr" to ::minCifrUp,
            "maxCifr" to ::maxCifrUp,)
        if (name in mapsFuncs.keys)
        {
            return mapsFuncs[name];
        }
        else
        {
            println("Некорректное название функции")
            return null;
        }
    }
    // Добавим третью функцию (Int)->Int
    // максимальная цифра числа рекурсией вверх
    fun maxCifrUp(n: Int): Int =
        if (n < 10) n else myMax(n % 10, maxCifrUp(n / 10));
    fun myMax(a: Int, b: Int) = if (a > b) a else b;

    // from 3
    // найти НОД двух чисел рекурсия вверх
    fun nodUp(a: Int, b: Int): Int = if (b > a) nodUpAkkum(b, a, a) else nodUpAkkum(a, b, b)
    fun nodUpAkkum(a: Int, b: Int, battery: Int): Int {
        if (battery == 1) return 1;
        else
        {
            var nextPossibleDel = battery - 1;
            var previousNod = nodUpAkkum(a, b, nextPossibleDel);
            return if ((a%battery==0)&&(b%battery==0)) battery else previousNod;
        }
    }
    // найти НОД двух чисел рекурсия вниз
    tailrec fun nodDown(a: Int, b: Int): Int =
        if (b == 0) a else if (b > a) nodDown(b, a) else nodDown(b, a%b)

    // произведение цифр числа, не делящихся на пять рекурсия вниз
    fun proizvCifrNotDelOnFiveDown(n: Int) = proizvCifrNotDelOnFiveDownAkkum(n, 1);
    tailrec fun proizvCifrNotDelOnFiveDownAkkum(n: Int, battery: Int): Int =
        if (n < 1)
            battery
        else proizvCifrNotDelOnFiveDownAkkum(n/10, battery * if ((n%10)%5!=0) n%10 else 1)
    // произведение цифр числа, не делящихся на пять рекурсия вверх
    fun proizvCifrNotDelOnFiveUp(n: Int): Int =
        if (n < 1)
            1
        else proizvCifrNotDelOnFiveUp(n/10) * if ((n%10)%5!=0) n%10 else 1

    // минимальная цифра числа рекурсией вниз
    fun minCifrDown(n: Int) = minCifrDownAkkum(n, n%10);
    tailrec fun minCifrDownAkkum(n: Int, battery: Int): Int =
        if (n < 1) battery else minCifrDownAkkum(n/10, myMin(n%10, battery));
    // минимальная цифра числа рекурсией вверх
    fun minCifrUp(n: Int): Int =
        if (n < 10) n else myMin(n % 10, minCifrUp(n / 10));
    fun myMin(a: Int, b: Int) = if (a < b) a else b;

    // from 2
    // найти НОД двух чисел
    fun nodLoop(a: Int, b: Int): Int {
        var canChanA = a;
        var canChanB = b;
        if (canChanB > canChanA)
        {
            val t = canChanA;
            canChanA = canChanB;
            canChanB = t;
        }
        var ostatok =  canChanA % canChanB;
        while (ostatok != 0)
        {
            canChanA = canChanB;
            canChanB = ostatok;
            ostatok =  canChanA % canChanB;
        }
        return canChanB;
    }
    // найти произведение цифр числа, не делящихся на пять
    fun proizvCifrNotDelOnFiveLoop(n: Int): Int {
        var proizv = 1;
        var cannableChangeN = n;
        while(cannableChangeN > 0)
        {
            val nextDigit = cannableChangeN % 10;
            if ((nextDigit % 5) != 0)
            {
                proizv *= nextDigit;
            }
            cannableChangeN /= 10;
        }
        return  proizv;
    }
    // получить минимальную цифру числа
    fun minCifrLoop(n: Int): Int {
        var minCifr = n % 10;
        var cannableChangeN = n / 10;
        while (cannableChangeN > 0)
        {
            val nextDigit = cannableChangeN % 10;
            if (nextDigit < minCifr)
            {
                minCifr = nextDigit;
            }
            cannableChangeN /= 10;
        }
        return minCifr
    }
}

fun main() = Main().main2();