import java.lang.System.`in`
import java.util.Scanner
import java.io.File

class Main() {
    fun main() {
        println("hi, Kotlin");
        val answer = findMaxPerimetr(1000)
        print("Answer is $answer")
    }
    fun findMaxPerimetr(perimetr: Int): Int {
        if (perimetr == 120) return 120
        else {
            println(perimetr);
            val previousMaxPerimetr = findMaxPerimetr(perimetr - 1)
            val currentAnswer = countSolutions(perimetr)
            val previousAnswer = countSolutions(previousMaxPerimetr)
            println(perimetr);
            return if (previousAnswer > currentAnswer) previousMaxPerimetr else perimetr
        }
    }
    fun findMaxSolution(perimetr: Int): Int {
        println(perimetr)
        if (perimetr == 120) return 3
        else
        {
            var maxi = myMax(findMaxSolution(perimetr-1), countSolutions(perimetr))
            println(perimetr);
            return maxi
        }
    }
    fun myMax(a: Int, b: Int) = if (a>b) a else b;
    fun countSolutions(perimetr: Int): Int =
        if (perimetr == 120) 3 else startCounting(1, perimetr)
    fun startCounting(a: Int, perimetr: Int): Int =
        if (a > perimetr-1) 0
        else startCounting2(a, 1, perimetr) + startCounting(a+1, perimetr);
    fun startCounting2(a: Int, b: Int, perimetr: Int): Int =
        if (b > perimetr-1) 0
        else (startCounting2(a, b+1, perimetr) + (if (prTreyg(a, b, perimetr)) 1 else 0));
    fun prTreyg(a: Int, b: Int, perimetr: Int): Boolean {
        // в эту функцию попадают оба случая вида 20 48 52 и 48 20 52, оба БУДУТ СЧИТАТЬСЯ
        // мы можем пренебречь этой неточностью в рамках текущей задачи)
        val c = (perimetr - a) - b;
        if ((a*a == (b*b + c*c)) ||
            (b*b == (a*a + c*c)) ||
            (c*c == (b*b + a*a))) {
            return true;
        }
        else
            return  false;
    }
}

fun main() = Main().main();