import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class MainTests {
    // 4
    @Test
    fun passProizvCifrInWayfarer() {
        val main = Main()
        val expected = 56
        assertEquals(expected, main.whatDoYouWantMyWayfarer(542075, main::proizvCifrNotDelOnFiveUp))
        assertEquals(expected, main.whatDoYouWantMyWayfarer(542075, main::proizvCifrNotDelOnFiveDown))
    }
    @Test
    fun passMinCifrInWayfarer() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.whatDoYouWantMyWayfarer(874328, main::minCifrUp))
        assertEquals(expected, main.whatDoYouWantMyWayfarer(874328, main::minCifrDown))
    }
    @Test
    fun passNodsInWayfarer() {
        val main = Main()
        val expected = 20
        assertEquals(expected, main.whatDoYouWantMyWayfarer2(1260, 122420, main::nodUp))
        assertEquals(expected, main.whatDoYouWantMyWayfarer2(1260, 122420, main::nodDown))
    }

    // 3
    @Test
    fun nodUp() {
        val main = Main()
        val expected = 20
        assertEquals(expected, main.nodUp(1260, 122420))
    }
    @Test
    fun nodDown() {
        val main = Main()
        val expected = 20
        assertEquals(expected, main.nodDown(1260, 122420))
    }

    @Test
    fun proizvCifrNotDelOnFiveDown() {
        val main = Main()
        val expected = 56
        assertEquals(expected, main.proizvCifrNotDelOnFiveDown(542075))
    }
    @Test
    fun proizvCifrNotDelOnFiveUp() {
        val main = Main()
        val expected = 56
        assertEquals(expected, main.proizvCifrNotDelOnFiveUp(542075))
    }

    @Test
    fun minCifrDown() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.minCifrUp(874328))
    }
    @Test
    fun minCifrUp() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.minCifrUp(874328))
    }

    // 2
    @Test
    fun nodLoop() {
        val main = Main()
        val expected = 20
        assertEquals(expected, main.nodLoop(1260, 122420))
    }

    @Test
    fun proizvCifrNotDelOnFiveLoop() {
        val main = Main()
        val expected = 56
        assertEquals(expected, main.proizvCifrNotDelOnFiveLoop(542075))
    }

    @Test
    fun minCifrLoop() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.minCifrLoop(874328))
    }

    // 1
    @Test
    fun max3() {
        val main = Main()
        val expected = 10
        assertEquals(expected, main.max3(3,10,5))
    }
    @Test
    fun factorialUp() {
        val main = Main()
        val expected = 720
        assertEquals(expected, main.factorialUp(6))
    }
    @Test
    fun factorialDown() {
        val main = Main()
        val expected = 720
        assertEquals(expected, main.factorialUp(6))
    }
    @Test
    fun sumCifrUp() {
        val main = Main()
        val expected = 11
        assertEquals(expected, main.sumCifrUp(2135))
    }
    @Test
    fun sumCifrDown() {
        val main = Main()
        val expected = 11
        assertEquals(expected, main.sumCifrDown(2135))
    }
    @Test
    fun getFunc1() {
        val main = Main()
        val expected = 15;
        assertEquals(expected, main.getFunc(true)(12345))
    }
    @Test
    fun realGetFunc1() {
        val main = Main()
        val expected = main::sumCifrUp;
        assertEquals(expected, main.getFunc(true))
    }
    @Test
    fun getFunc2() {
        val main = Main()
        val expected = 720;
        assertEquals(expected, main.getFunc(false)(6))
    }
    @Test
    fun realGetFunc2() {
        val main = Main()
        val expected = main::factorialUp;
        assertEquals(expected, main.getFunc(false))
    }
    @Test
    fun sumd() {
        val main = Main()
        val expected = 15
        assertEquals(expected, main.sumd(12345))
    }
    @Test
    fun muld() {
        val main = Main()
        val expected = 126
        assertEquals(expected, main.muld(367))
    }
    @Test
    fun maxd() {
        val main = Main()
        val expected = 7
        assertEquals(expected, main.maxd(123745))
    }
    @Test
    fun mind() {
        val main = Main()
        val expected = 2
        assertEquals(expected, main.mind(923745))
    }
}