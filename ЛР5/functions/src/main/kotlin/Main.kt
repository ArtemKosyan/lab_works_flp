class Main() {
    // entry point
    fun main() {
        println("Start");
        val x1: Int = 12345;
        var flag = true;
        println(getFunc(flag)(x1))
        val x2: Int = 6;
        flag = false;
        println(getFunc(flag)(x2))
        println("End");
    }
    // 4
    fun whatDoYouWantMyWayfarer2(a: Int, b:Int, func2: (Int, Int)->Int): Int =
        func2(a, b);
    fun whatDoYouWantMyWayfarer(n: Int, func: (Int)->Int): Int =
        func(n);

    // 3
    // найти НОД двух чисел рекурсия вверх
    fun nodUp(a: Int, b: Int): Int = if (b > a) nodUpAkkum(b, a, a) else nodUpAkkum(a, b, b)
    fun nodUpAkkum(a: Int, b: Int, battery: Int): Int {
        if (battery == 1) return 1;
        else
        {
            var nextPossibleDel = battery - 1;
            var previousNod = nodUpAkkum(a, b, nextPossibleDel);
            return if ((a%battery==0)&&(b%battery==0)) battery else previousNod;
        }
    }
    // найти НОД двух чисел рекурсия вниз
    tailrec fun nodDown(a: Int, b: Int): Int =
        if (b == 0) a else if (b > a) nodDown(b, a) else nodDown(b, a%b)

    // произведение цифр числа, не делящихся на пять рекурсия вниз
    fun proizvCifrNotDelOnFiveDown(n: Int) = proizvCifrNotDelOnFiveDownAkkum(n, 1);
    tailrec fun proizvCifrNotDelOnFiveDownAkkum(n: Int, battery: Int): Int =
        if (n < 1)
            battery
        else proizvCifrNotDelOnFiveDownAkkum(n/10, battery * if ((n%10)%5!=0) n%10 else 1)
    // произведение цифр числа, не делящихся на пять рекурсия вверх
    fun proizvCifrNotDelOnFiveUp(n: Int): Int =
        if (n < 1)
            1
        else proizvCifrNotDelOnFiveUp(n/10) * if ((n%10)%5!=0) n%10 else 1

    // минимальная цифра числа рекурсией вниз
    fun minCifrDown(n: Int) = minCifrDownAkkum(n, n%10);
    tailrec fun minCifrDownAkkum(n: Int, battery: Int): Int =
        if (n < 1) battery else minCifrDownAkkum(n/10, myMin(n%10, battery));
    // минимальная цифра числа рекурсией вверх
    fun minCifrUp(n: Int): Int =
        if (n < 10) n else myMin(n % 10, minCifrUp(n / 10));
    fun myMin(a: Int, b: Int) = if (a < b) a else b;

    // 2
    // найти НОД двух чисел
    fun nodLoop(a: Int, b: Int): Int {
        var canChanA = a;
        var canChanB = b;
        if (canChanB > canChanA)
        {
            val t = canChanA;
            canChanA = canChanB;
            canChanB = t;
        }
        var ostatok =  canChanA % canChanB;
        while (ostatok != 0)
        {
            canChanA = canChanB;
            canChanB = ostatok;
            ostatok =  canChanA % canChanB;
        }
        return canChanB;
    }

    // найти произведение цифр числа, не делящихся на пять
    fun proizvCifrNotDelOnFiveLoop(n: Int): Int {
        var proizv = 1;
        var cannableChangeN = n;
        while(cannableChangeN > 0)
        {
            val nextDigit = cannableChangeN % 10;
            if ((nextDigit % 5) != 0)
            {
                proizv *= nextDigit;
            }
            cannableChangeN /= 10;
        }
        return  proizv;
    }

    // получить минимальную цифру числа
    fun minCifrLoop(n: Int): Int {
        var minCifr = n % 10;
        var cannableChangeN = n / 10;
        while (cannableChangeN > 0)
        {
            val nextDigit = cannableChangeN % 10;
            if (nextDigit < minCifr)
            {
                minCifr = nextDigit;
            }
            cannableChangeN /= 10;
        }
        return minCifr
    }


    // 1
    //функция высшего порядка принимает функцию
    tailrec fun digits(n: Int, a: Int = 0, f: (Int, Int) -> Int): Int =
        if (n == 0) a else digits(n / 10, f(a, n % 10), f)
    //вызовы через лямбды
    fun sumd(n: Int): Int = digits(n, 0) { a, b -> (a + b) }
    fun muld(n: Int): Int = digits(n, 1) { a, b -> (a * b) }
    fun maxd(n: Int): Int = digits(n / 10, n % 10) { a, b -> if (a > b) a else b }
    fun mind(n: Int): Int = digits(n / 10, n % 10) { a, b -> if (a < b) a else b }
    // получить функцию по логическому значению
    fun getFunc(flag: Boolean): (Int)->Int =
        if (flag) ::sumCifrUp else ::factorialUp;
    // сумма цифр вниз
    fun sumCifrDown(n: Int): Int = sumCifrDownAkkum(n, 0);
    tailrec fun sumCifrDownAkkum(n: Int, battery: Int):Int =
        if (n < 1) battery else sumCifrDownAkkum(n/10, (battery+n%10));
    // сумма цифр вверх
    fun sumCifrUp(n: Int): Int =
        if (n < 10) n else (n % 10) + sumCifrUp(n / 10);
    // факториал вниз
    fun factorialDown(n: Int): Int = factDownAkkum(n, 1);
    tailrec fun factDownAkkum(n: Int, battery: Int): Int =
        if (n <= 1) n * battery else factDownAkkum(n - 1, n * battery);
    // факториал вверх
    fun factorialUp(n: Int): Int =
        if (n <= 1) 1 else factorialUp(n-1) * n;
    // get max between X Y Z
    fun max3(X: Int, Y: Int, Z: Int) =
        if (X > Y)
            if (X > Z) X else Z;
        else
            if (Y > Z) Y else Z;

}

fun main() = Main().main();