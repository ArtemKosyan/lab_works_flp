/* task 1 */

/* fact(+N, -X) unifies X as a factorial of N */
fact(0, 1) :- !.
fact(N, X) :- LastN is N-1, fact(LastN, LastX), X is LastX*N.

/* fact_down(+N, -X) unifies X as a factorial of N */
fact_down(N, X) :- NewN is N-1, fact_down(NewN, N, X).
fact_down(0, X, X) :- !.
fact_down(N, Fact, X) :- NewFact is Fact*N, LastN is N-1, fact_down(LastN, NewFact, X).

/* sum_cifr(+N, ?Sum) unifies Sum as the sum of the digits of the number N
   or checks whether Sum is the sum of the digits of the number N */
sum_cifr(0, 0) :- !.
sum_cifr(N, Sum) :- LessN is N div 10, sum_cifr(LessN, OldSum), Digit is N mod 10, Sum is OldSum+Digit.
/* sum_cifr_down(+N, ?Sum) unifies Sum as the sum of the digits of the number N  
   or checks whether Sum is the sum of the digits of the number N */
sum_cifr_down(N, Sum) :- sum_cifr_down(N, 0, Sum).
sum_cifr_down(0, Sum, Sum) :- !.
sum_cifr_down(N, CurSum, Sum) :- 
	LessN is N div 10,
	Digit is N mod 10,
	NewCurSum is CurSum+Digit,
	sum_cifr_down(LessN, NewCurSum, Sum).

/* free_of_squares(+N) answers whether the number N is square-free */
free_of_squares(N) :- free_of_squares(N, N).
free_of_squares(_, 2) :- !.
free_of_squares(N, DelN) :-
	is_del(N, DelN)
		     ->
		     (not(is_square(DelN)) 
					->
					(NewDelN is DelN-1, free_of_squares(N, NewDelN))
					;
					fail)
		     ;
		     (NewDelN is DelN-1, free_of_squares(N, NewDelN)).
/* is_square(+K) answers whether the number K is square of natural number */
is_square(K) :- MaxDel is K div 2, is_square(K, MaxDel).
is_square(_, 1) :- !, fail.
is_square(K, DelK) :- K is DelK*DelK -> true; NewDelK is DelK-1, is_square(K, NewDelK).
/* is_del(+N, +Del) checks whether Del is delitel of N*/
is_del(N, Del) :- Ostatok is N mod Del, Ostatok is 0.

/* read_list(+N, -List) is reading N-elements and specifies List with these elements*/
read_list(0, []) :- !.
read_list(N, [Head|Tail]) :- read(Head), NewN is N-1, read_list(NewN,Tail).
/* write_list(+List) outputs each element of List from a new line*/
write_list([]) :- !.
write_list([H|T]) :- write(H), nl, write_list(T).

/* main_for_sum_list() - entry point for finding sum of list items */
main_for_sum_list() :- 
	write("How much elements in list?"), nl, read(N), 
	write("write every element with '.'"), nl, read_list(N, List),
	sum_list_down(List, Sum),
	write("Your sum is:"), write(Sum).
/* sum_list_down(+List, ?Summ) */
sum_list_down(List, Summ) :- sum_list_down(List, 0, Summ).
sum_list_down([], CurSum, CurSum) :- !.
sum_list_down([H|T], CurSum, Sum) :- NewSum is CurSum + H, sum_list_down(T, NewSum, Sum).
/* sum_list_up(+List, ?Summ) */
sum_list_up([], 0) :- !.
sum_list_up([H|T], Summ) :- sum_list_up(T,SumTail), Summ is SumTail + H.

/* remove_elements(+List, +SumCifr) removes elements from List whose sum of digits is equal to SumCifr
   and print new list */
remove_elements(List, SumCifr) :- rm_cif(List, CorrectedList, SumCifr), write_list(CorrectedList), !.
rm_cif([], [], _) :- !.
rm_cif([H|T], X, N) :- sum_cifr(H, S), S is N, rm_cif(T, X, N).
rm_cif([H|X], [H|Y], N) :- sum_cifr(H, S), not(S is N), rm_cif(X, Y, N).

/* task2 */

/* find_min_cifr(+N, -Digit) unifies Digit as the minimum digit of the number N */
find_min_cifr(LastDigit, LastDigit) :- is_digit(LastDigit), !.
find_min_cifr(N, Digit) :-
	LessN is N div 10,
	find_min_cifr(LessN, OtherDigit),
	SomeDigit is N mod 10,
	min(OtherDigit, SomeDigit, Digit).
/* min(+X, +Y, -Z) return Z as minimum between X and Y */
min(X, Y, Z) :- X<Y -> (Z is X); Z is Y.
/* is_digit(+N) return true if the number belongs to a set {0, 1, 2, 3, 4, 5, 6, 7, 8, 9} */
is_digit(N) :- N>=0, N=<9.
/* find_min_cifr_down(+N, -Digit) unifies Digit as the minimum digit of the number N */
find_min_cifr_down(N, Digit) :- SomeDigit is N mod 10, find_min_cifr_down(N, SomeDigit, Digit).
find_min_cifr_down(0, CurMinDigit, CurMinDigit) :- !.
find_min_cifr_down(N, CurMinDigit, Digit) :- 
	SomeDigit is N mod 10,
	LessN is N div 10,
	min(SomeDigit, CurMinDigit, NewMinDigit),
	find_min_cifr_down(LessN, NewMinDigit, Digit).

/* proizv_cifr_notdel_five(+N, -Proizv) unifies Proizv as product of digits of number N
   that are not divisible by five */
proizv_cifr_notdel_five(Digit, RecursionDno) :- 
	is_digit(Digit), 
	(is_del_on_five(Digit) -> (RecursionDno is 1); (RecursionDno is Digit)), !.
proizv_cifr_notdel_five(N, Proizv) :- 
	LessN is N div 10,
	proizv_cifr_notdel_five(LessN, OldProizv),
	PossibleMnozh is N mod 10,
	(not(is_del_on_five(PossibleMnozh)) -> (Proizv is OldProizv*PossibleMnozh); (Proizv is OldProizv)).
/* is_del_on_five(+X) return true if X is divisible by five without remainder  */
is_del_on_five(X) :- Ostatok is X mod 5, Ostatok is 0.
/* proizv_cifr_notdel_five_down(+N, -Proizv) */
proizv_cifr_notdel_five_down(N, Proizv) :- proizv_cifr_notdel_five_down(N, 1, Proizv).
proizv_cifr_notdel_five_down(0, CurProizv, CurProizv) :- !.
proizv_cifr_notdel_five_down(N, CurProizv, Proizv) :-
	PossibleMnozh is N mod 10,
	LessN is N div 10,
	(not(is_del_on_five(PossibleMnozh)) -> NewCurProizv is CurProizv*PossibleMnozh; NewCurProizv is CurProizv),
	proizv_cifr_notdel_five_down(LessN, NewCurProizv, Proizv).


/* nod(+X, +Y, -Z) specifies Z as the greatest common divisor of the numbers X and Y*/
nod(X, 0, X) :- !.
nod(X, Y, Z) :- (X < Y -> nod(Y, X, Z); R is X mod Y, nod(Y, R, Z)).
/* nod_up(+X, +Y, -Z) specifies Z as the greatest common divisor of the numbers X and Y*/
nod_up(X, Y, Z) :- (X < Y -> nod_up(Y, X, X, Z); nod_up(X, Y, Y, Z)).
nod_up(_, _, 1, 1) :- !.
nod_up(X, Y, T, Z) :- 
	NewT is T-1,
	nod_up(X, Y, NewT, CurZ),
	(is_del(X, T),
	is_del(Y, T),
	Z is T, !; Z is CurZ).

/* dels(+X, -Y) enumerates Y as delitels of X */
dels(X, Y) :- dels(X, 1, Y).
dels(X, X, X) :- !.
dels(X, Z, Y) :- is_del(X, Z), Y is Z;
	NewZ is Z+1, dels(X, NewZ, Y).

/* task 3 */

/* main_before_last_min() - entry point for task of searching for list items located before last minimum */
main_before_last_min() :- write("How much elements in list?"), nl, read(N),
	write("write every element with '.'"), nl, read_list(N, List), 
	find_min_in_list(List, Min),
	get_list_before_last_min(List, Min, NewList),
	write("Your answer list is:"), write(NewList).
/* find_min_in_list(+List, -Min) finds the last minimum element among the list items */
find_min_in_list([H], H) :- !.
find_min_in_list([H|T], Min) :-
	find_min_in_list(T, NewMin),
	min(H, NewMin, Min).
/* get_list_before_last_min(+List, +Min, -NewList) unifies NewList as list of items located before last Min in List*/
get_list_before_last_min(List, Min, NewList) :-
	reverse(List, ReversedList),
	get_list_after_first_min(ReversedList, Min, NewList2),
	reverse(NewList2, NewList).
/* get_list_after_first_min(+ReversedList, +Min, -NewList) unifies NewList as list of items
   located after first Min in ReversedList */
get_list_after_first_min([H|T], Min, NewList) :-
	(H is Min -> NewList=T, ! ; get_list_after_first_min(T, Min, NewList)).

/* we assume that there are two or more IDENTICAL maximum elements in the list */
/* main_get_elements_between_first_and_second_max() - entry point for task of searching list items
   located between first and second maximum elements */
main_get_elements_between_first_and_second_max() :- 
	write("How much elements in list?"), nl, read(N),
	write("write every element with '.'"), nl, read_list(N, List), 
	find_max_in_list(List, Max),
	get_elements_between_first_and_second_max(List, Max, NewList),
	write("Your answer list is:"), write(NewList).
/* find_max_in_list(+List, -Max) finds the last maximum element among items of List */
find_max_in_list([H], H) :- !.
find_max_in_list([H|T], Max) :-
	find_max_in_list(T, NewMax),
	max(H, NewMax, Max).
/* max(+X, +Y, -Z) return Z as maximum between X and Y */
max(X, Y, Z) :- X>Y -> (Z is X); Z is Y.
/* get_elements_between_first_and_second_max(+List, +Max, -NewList) unifies NewList
   as list of elements from List located between first and second maximum */
get_elements_between_first_and_second_max(List, Max, NewList) :-
	skip_elements_before_first_max(List, Max, NewList2),
	take_elements_before_first_max(NewList2, Max, NewList).	
/* skip_elements_before_first_max(+List, +Max, -NewList) specifies NewList
   as list with items from List after first maximum element */
skip_elements_before_first_max([_], _, NewList) :- NewList=[], !.
skip_elements_before_first_max([H|T], Max, NewList) :-
	(H is Max -> (NewList=T); skip_elements_before_first_max(T, Max, NewList)).
/* take_elements_before_first_max(+List, +Max, -NewList) specifies NewList
   as list with items from List before first maximum element */
take_elements_before_first_max([_], _, []) :- !.
take_elements_before_first_max([H|T], Max, NewList) :-
	(H is Max -> (NewList=[]); (take_elements_before_first_max(T, Max, CurList), NewList=[H|CurList])).

/* main_between_first_and_last_min() */
main_between_first_and_last_min() :- write("How much elements in list?"), nl, read(N),
	write("write every element with '.'"), nl, read_list(N, List), 
	find_min_in_list(List, Min),
	between_first_and_last_min(List, Min, NewList),
	number_elements(NewList, Number),	
	write("Your answer number elements between first and last minimum is:"), write(Number).
/* between_first_and_last_min(+List, +Min, -NewList) specifies NewList as list with items
   from List between first and last minimum elements*/
between_first_and_last_min(List, Min, NewList) :-
	get_list_after_first_min(List, Min, NewList2),
	reverse(NewList2, ReversedNewList2),
	get_list_after_first_min(ReversedNewList2, Min, NewList3),
	reverse(NewList3, NewList).
/* number_elements(+List, -N) specifies N as length of List */
number_elements([], 0) :- !.
number_elements([_], 1) :- !.
number_elements([_|T], N) :- number_elements(T, OldN), N is OldN+1.