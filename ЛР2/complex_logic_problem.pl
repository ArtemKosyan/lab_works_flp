/* in_list(+List, ?Item):
   +List, -Item -- iterates through Item as elements of List
   +List, +Item -- waits for a List with anonymous variables
   	and iterates over placement of element Item according
	to the positions of anonymous variables in the List */
in_list([El|_],El).
in_list([_|T],El):-in_list(T,El).

/* workers() - entry point for solving logical problem with three people:
   Aladar, Belaya and Balash.*/
workers() :-
	Humans = [_, _, _],
	/* three humans , three professions , three towns */
	in_list(Humans, [aladar, Prof1, Town1]),
	in_list(Humans, [beloy, Prof2, Town2]),
	in_list(Humans, [balash, Prof3, Town3]),

	in_list(Humans, [_, aptekar, _]),
	in_list(Humans, [_, bugalter, _]),
	in_list(Humans, [_, agronom, _]),

	in_list(Humans, [_, _, budapesht]),
	in_list(Humans, [_, _, bekesh]),
	in_list(Humans, [_, _, asoda]),

	/* balash redko bivaet v budapehte */
	Town3 \= budapesht,

	/* wife of aptekar is sister balasha so balash not aptekar */
	Prof3 \= aptekar,

	/* sister of balash is wife of aptekar but sister of balash live in budapesht */
	in_list(Humans, [_, aptekar, budapesht]),

	/* for two people, names of their professions and cities, in which they live,
	   begin with same letter as their names */
	(
	 (
		( in_list(Humans, [aladar, aptekar, asoda]); in_list(Humans, [aladar, agronom, asoda]) ),
		( in_list(Humans, [beloy, bugalter, budapesht]); in_list(Humans, [beloy, bugalter, bekesh]) )
	 )
		;
	 (
		( in_list(Humans, [aladar, aptekar, asoda]) ; in_list(Humans, [aladar, agronom, asoda]) ),
		( in_list(Humans, [balash, bugalter, budapesht]); in_list(Humans, [balash, bugalter, bekesh]) )
	 )
	),
	!,
	write(Humans), nl.
