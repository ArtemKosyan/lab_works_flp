/* task 5 */

/* maximum_prime_divisor(+N, -Divisor) unifies Divisor as maximum prime divisor of N*/
maximum_prime_divisor(N, Divisor) :- 
	dels(N, Divisor),
	prostoe(Divisor),
	!.
/* prostoe(+N) return true if N prostoe number */
prostoe(1) :- !, fail.
prostoe(N) :- CurDel is N div 2, prostoe(N, CurDel).
prostoe(_, 1) :- !.
prostoe(N, CurDel) :- 
	( (CurDel\=1, is_del(N, CurDel)) -> (!, fail) ; (NextDel is CurDel-1, prostoe(N, NextDel)) ).
/* dels(+X, -Y) enumerates Y as delitels of X starting from maximum divisor of X*/
dels(X, Y) :- dels(X, X, Y).
dels(_, 1, 1) :- !.
dels(X, Z, Y) :- is_del(X, Z), Y is Z;
	NewZ is Z-1, dels(X, NewZ, Y).
/* is_del(+N, +Del) checks whether Del is delitel of N*/
is_del(N, Del) :- Ostatok is N mod Del, Ostatok is 0.

/* example: nod_of_max_nechet_neprost_del_and_proizv_cifr(66, Answer). Answer is 3. */
/* nod_of_max_nechet_neprost_del_and_proizv_cifr(+N, -Answer) unifies Answer as nod of (maximum nechet neprostoe divisor of N, proizv cifr of N) */
nod_of_max_nechet_neprost_del_and_proizv_cifr(N, Answer) :-
	max_nechet_neprost_del(N, Del),
	proizv_cifr(N, Proizv),
	nod(Del, Proizv, Answer).
/* max_nechet_neprost_del(+N, -Del) specifies Del as maximum nechet neprostoy divisor of N */
max_nechet_neprost_del(N, Del) :-
	dels(N, Del),
	not(prostoe(Del)),
	is_nechet(Del),
	!.
/* is_nechet(+N) return true if N is prime number */
is_nechet(N) :- Ostatok is N mod 2, Ostatok is 1.
/* proizv_cifr(+N, -Proizv) specifies Proizv as product of digits of number N*/
proizv_cifr(Digit, RecursionDno) :- 
	is_digit(Digit),
	RecursionDno is Digit, 
	!.
proizv_cifr(N, Proizv) :- 
	LessN is N div 10,
	proizv_cifr(LessN, OldProizv),
	Mnozh is N mod 10,
	Proizv is OldProizv*Mnozh.
/* is_digit(+N) return true if the number belongs to a set {0, 1, 2, 3, 4, 5, 6, 7, 8, 9} */
is_digit(N) :- N>=0, N=<9.
/* nod(+X, +Y, -Z) specifies Z as the greatest common divisor of the numbers X and Y*/
nod(X, 0, X) :- !.
nod(X, Y, Z) :- (X < Y -> nod(Y, X, Z); R is X mod Y, nod(Y, R, Z)).

/* task 7 */

/* read_list(+N, -List) is reading N-elements and specifies List with these elements*/
read_list(0, []) :- !.
read_list(N, [Head|Tail]) :- read(Head), NewN is N-1, read_list(NewN,Tail).
/* write_list(+List) outputs each element of List from a new line */
write_list([]) :- !.
write_list([H|T]) :- write(H), nl, write_list(T).

/* main_write_chet_nechet_index_lists() - entry point
 for printing list items in odd and even positions. */
main_write_chet_nechet_index_lists() :-
	write("How much elements in list?"), nl, read(N),
	write("write every element with '.'"), nl, read_list(N, List), 
	get_chet_and_nechet_index_Lists(List, NechetList, ChetList),
	write("Your answer lists is:"), nl,
	write("List of nechetnie elements = "), write(NechetList), nl,
	write("List of chetnie elements = "), write(ChetList). 
/* get_chet_and_nechet_index_Lists(+List, -NechetList, -ChetList) -
 specifies NechetList and ChetList with elements from the List in odd and even positions */
get_chet_and_nechet_index_Lists([], [], []) :- !.
get_chet_and_nechet_index_Lists([H], [H], []) :- !.
get_chet_and_nechet_index_Lists([H, T], [H], [T]) :- !.
get_chet_and_nechet_index_Lists(List, NechetList, ChetList) :-
	number_elements(List, Leength),
	Leeength is Leength+1,
	get_chet_and_nechet_index_Lists(List, NechetList, ChetList, Leeength).
/* get_chet_and_nechet_index_Lists(+List, -NechetList, -ChetList, +N) - 
 N should be the number of elements in the List +1
 specifies NechetList and ChetList with elements from the List in odd and even positions  */
get_chet_and_nechet_index_Lists([H,T], NechetList, ChetList, 3) :-
	NechetList=[H],
	ChetList=[T],
	!.
get_chet_and_nechet_index_Lists([H|T], NechetList, ChetList, N) :-
	OldN is N-1,
	get_chet_and_nechet_index_Lists(T, OldNechetList, OldChetList, OldN),
	(     (is_nechet(N))
			-> 
			( NechetList=[H|OldNechetList], ChetList=OldChetList)
			;
			( ChetList=[H|OldChetList], NechetList=OldNechetList)
	).
/* number_elements(+List, -Number) specifies Number as number of elements in the List */
number_elements([], 0) :- !.
number_elements([_], 1) :- !.
number_elements([_|T], N) :- number_elements(T, OldN), N is OldN+1.

/* main_get_halfset_of_divisors_of_items_from_list() - entry point 
 for getting divisors of list items without repetition. */
main_get_halfset_of_divisors_of_items_from_list() :-
	write("How much elements in list?"), nl, read(N),
	write("write every element with '.'"), nl, read_list(N, List), 
	get_halfset_of_divisors_of_items_from_list(List, HalfSet),
	write("Your answer lists is:"), nl,
	write("HalfSet of divisors of elements = "), write(HalfSet). 
/* get_halfset_of_divisors_of_items_from_list(+List, -DivisorsList) -
 specifies DivisorsList as unique divisors of elements from List  */
get_halfset_of_divisors_of_items_from_list([], []) :- !.
get_halfset_of_divisors_of_items_from_list([H|T], HalfSet) :-
	get_halfset_of_divisors_of_items_from_list(T, LastHalfSet),
	update_halfset(H, LastHalfSet, HalfSet).	
/* update_halfset(+Element, +LastHalfSet, -NewHalfSet) specifies
 NewHalfSet as union of LastHalfSet and divisors of Element that were not in LastHalfSet */
update_halfset(Element, LastHalfSet, NewHalfSet) :-
	go_check_all_divisors(Element, Element, LastHalfSet, NewHalfSet).
/* go_check_all_divisors(+Element, +CurDdel, +LastHalfSet, -NewHalfSet) -
 checking if CurDel is divisor of Element then NewHalfSet is union of Curdel and LastHalfSet
 else NewHalfSet is LastHalfSet */
go_check_all_divisors(_, 0, LastHalfSet, LastHalfSet) :- !.
go_check_all_divisors(Element, N, LastHalfSet, HalfSet) :-
	PrevNumber is N-1,
	go_check_all_divisors(Element, PrevNumber, LastHalfSet, OldHalfSet),
	( 
	 is_del(Element, N)
			 ->
			 ( is_contains_item(OldHalfSet, N)
							->
							(HalfSet=OldHalfSet)
							;
							(HalfSet=[N|OldHalfSet])

			 )
			 ;
			 ( HalfSet=OldHalfSet )
	).
/* is_contains_item(+List, +Elem) return true if Elem is item of List */
is_contains_item([], _) :- fail, !.
is_contains_item([H|T], Item) :-
	( H is Item, ! )
	;
	( is_contains_item(T, Item) ).


/*  */
main_get_lists_halfset_of_items_and_their_counters() :-
	write("How much elements in list?"), nl, read(N),
	write("write every element with '.'"), nl, read_list(N, List), 
	get_lists_halfset_of_items_and_their_counters(List, HalfSet, Counters),
	write("Your answer lists is:"), nl,
	write("Half-set of items = "), write(HalfSet), nl,
	write("Counters of items = "), write(Counters).
/* get_lists_halfset_of_items_and_their_counters(+List, -HalfSet, -Counters) -
 specifies HalfSet as unique items from List and specifies Counters as
 list of number of occurrences of each unique item */
get_lists_halfset_of_items_and_their_counters([], [], []) :- !.
get_lists_halfset_of_items_and_their_counters([H|T], HalfSet, Counters) :-
	get_lists_halfset_of_items_and_their_counters(T, OldHalfSet, OldCounters),
	(  is_contains_item(OldHalfSet, H)
					->
					(
						get_index_of_item(OldHalfSet, H, Index),
						plus_counter_on_index(OldCounters, Index, Counters),
						HalfSet = OldHalfSet
					)
					;
					(HalfSet=[H|OldHalfSet], Counters=[1|OldCounters])
	).
/* get_index_of_item(+List, +Element, -Index) - 
 specifies Index as position number of LAST occurrence of Element in List, the count starts from 1*/
get_index_of_item(List, H, Index) :-
	number_elements(List, Leength),
	reverse(List, ReversedList),
	get_index_of_item(ReversedList, H, Index, Leength).
/* get_index_of_item(+List, +Item, +CurIndex, -N) - specifies N as CurIndex if Item is head of List */
get_index_of_item([Item], Item, 1, 1) :- !.
get_index_of_item([H|T], Item, Index, N) :-
	( H is Item
		 ->
		 (Index is N)
		 ;
		 (
			OldN is N-1,
			get_index_of_item(T, Item, Index, OldN)			
		 )
	).	
/* plus_counter_on_index(+List, +Index, -UpdatedList) - adds one to the List item at position Index
 and pushes the result list into UpdatedList */
plus_counter_on_index([H|T], Index, UpdatedList) :- 
	( Index is 1
		  ->
		  ( 
			NewH is H+1,
			UpdatedList=[NewH|T]
		  )
		  ;
		  (
			NextIndex is Index-1,
			plus_counter_on_index(T, NextIndex, OldUpdatedList),
			UpdatedList=[H|OldUpdatedList]
		  )
	).
	
	
/* main_sort_list_by_frequency() - entry point for sorting list by frequency of
 occurrence of the element*/
main_sort_list_by_frequency() :-
	write("How much elements in list?"), nl, read(N),
	write("write every element with '.'"), nl, read_list(N, List), 
	get_lists_halfset_of_items_and_their_counters(List, HalfSet, Counters),
	sort_list_by_frequency(HalfSet, Counters, FrequencySortedList),
	write("Your answer lists is:"), nl,
	write("sorted list = "), write(FrequencySortedList).

/* sort_list_by_frequency(+UniqueElements, +CountersOfUniqueElements, -FrequencySortedList) - requires
 UniqueElements - list of unique items of some source list 
 CountersOfUniqueElements - list with counters for every unique item in UniqueElements
 so we can to specify FrequencySortedList as elements from UniqueElements decreasing in frequency,
 and they occur as many times as indicated by their counter in CountersOfUniqueElements */
sort_list_by_frequency(HalfSet, Counters, FrequencySortedList) :-
	number_elements(HalfSet, Leength),
	sort_list_by_frequency(HalfSet, Counters, FrequencySortedList, Leength).
/* sort_list_by_frequency(+List, +OtherList, -Result, +N) */
sort_list_by_frequency(_, _, [], 0) :- !.
sort_list_by_frequency(HalfSet, Counters, FrequencySortedList, Leength) :-
	PrevNomer is Leength-1,
	find_max_in_list(Counters, MaxCounter),
	get_index_of_item(Counters, MaxCounter, IndexOfMaxCounterAndNumber),
	get_item_by_index(HalfSet, IndexOfMaxCounterAndNumber, NumberNeedToRepeat),
	delete_item_by_index(HalfSet, IndexOfMaxCounterAndNumber, UpdatedHalfSet),
	delete_item_by_index(Counters, IndexOfMaxCounterAndNumber, UpdatedCounters),
	sort_list_by_frequency(UpdatedHalfSet, UpdatedCounters, OldFrequencySortedList, PrevNomer),
	add_number_by_repeated_count(OldFrequencySortedList, MaxCounter, NumberNeedToRepeat, FrequencySortedList).
/* delete_item_by_index(+List, +Index, -NewList) - specifies NewList as List without item on position Index 
 (begin from 1) */
delete_item_by_index(List, Index, ListDeletedItem) :-
	reverse(List, ReversedList),
	number_elements(List, Leength),
	delete_item_by_index(ReversedList, Index, ReversedListDeletedItem, Leength),
	reverse(ReversedListDeletedItem, ListDeletedItem).
/* delete_item_by_index(+List, +Index, -NewList, +CurViewIndex) */
delete_item_by_index([_|T], Index, T, Index) :- !.
delete_item_by_index([H|T], Index, ListDeletedItem, CurViewIndex) :-
	NextCurViewIndex is CurViewIndex-1,
	delete_item_by_index(T, Index, PrevListDeletedItem, NextCurViewIndex),
	ListDeletedItem=[H|PrevListDeletedItem].
/* add_number_by_repeated_count(+OldList, +Counter, +Element, -NewList) -
 specifies NewList as OldList by adding Element in the number of Counter units at the end */
add_number_by_repeated_count(OldFrequencySortedList, 0, _, OldFrequencySortedList) :- !.
add_number_by_repeated_count(OldFrequencySortedList, MaxCounter, NumberNeedToRepeat, FrequencySortedList) :-
	LessCounter is MaxCounter-1,
	add_number_by_repeated_count(OldFrequencySortedList, LessCounter, NumberNeedToRepeat, PreviousFrequencySortedList),
	FrequencySortedList = [NumberNeedToRepeat|PreviousFrequencySortedList].
/* get_item_by_index(+List, +Index, -Item) - indexing starts from 1,
 specifies Item as item in List on position Index */
get_item_by_index(List, Index, Item) :-
	reverse(List, ReversedList),
	number_elements(List, Leength),
	get_item_by_index(ReversedList, Index, Item, Leength).
/* get_item_by_index(+List, +Index, -Item, +CurIndex) */
get_item_by_index([H], 1, H, 1) :- !.
get_item_by_index([H|T], Index, Item, CurViewIndex) :-
	( Index is CurViewIndex
		  ->
		  ( 
			Item is H
		  )
		  ;
		  (
			NextCurViewIndex is CurViewIndex-1,
			get_item_by_index(T, Index, Item, NextCurViewIndex)
		  )
	).	
/* find_max_in_list(+List, -Max) finds the last maximum element among items of List */
find_max_in_list([H], H) :- !.
find_max_in_list([H|T], Max) :-
	find_max_in_list(T, NewMax),
	max(H, NewMax, Max).	
/* max(+X, +Y, -Z) return Z as maximum between X and Y */
max(X, Y, Z) :- X>Y -> (Z is X); Z is Y.






