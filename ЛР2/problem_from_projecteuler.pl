/* from 500 to 867 - 2 hours and 33 minutes */
/* from 500 to 962 - 4 hours and 00 minutes */
/* calculation from 500 to 1000 took 4 hours and 53 minutes and 51 seconds */

/* task 6 */

find_MAX_among_solutions(120, 3, 120) :- !.
find_MAX_among_solutions(P, Answer, AnswerP) :-
	OldP is P - 1,
	write(P), nl,
	find_MAX_among_solutions(OldP, OldAnswer, OldAnswerP),
	write(P), write(" "), write(OldAnswerP), nl,
	find_count_of_solutions(P, AnswerNow),	
	Answer is max(OldAnswer, AnswerNow),
	( (OldAnswer > AnswerNow) -> (AnswerP is OldAnswerP); (AnswerP is P) ).	
/* max(+X, +Y, -Z) return Z as maximum between X and Y */
max(X, Y, Z) :- X>Y -> (Z is X); Z is Y.

/* */
find_count_of_solutions(P, Count_of_solutions) :- 
	retractall(maxil(_)),
	perebor_vseh_troek_chisel_with_sum(P),
	how_much_facts_we_have(Maxim),
	Count_of_solutions is Maxim/6,
	!.
/* */
how_much_facts_we_have(Count) :-
    assertz(num_of_facts(0)),
    go_to_count(Count).
/* */
go_to_count(_) :-
    maxil(_),
    retract(num_of_facts(C)),
    C1 is C + 1,
    assertz(num_of_facts(C1)),
    fail.
go_to_count(Count) :-
    retract(num_of_facts(Count)).

/* */
perebor_vseh_troek_chisel_with_sum(P) :-
	perebor_do(P, A),
	PosMaxAvailableB is P-A,
	((PosMaxAvailableB =< 1) -> (MaxAvailableB is 1) ; (MaxAvailableB is PosMaxAvailableB)),
	perebor_do(MaxAvailableB, B),
	PosMaxAvailableC is (P-A)-B,
	((PosMaxAvailableC =< 1) -> (MaxAvailableC is 1) ; (MaxAvailableC is PosMaxAvailableC)),
	perebor_do(MaxAvailableC, C),
% write(A), write(" - "), write(B), write(" - "), write(C), nl,
	(
		(P is A+B+C,  
		 is_triangle(A, B, C),
		 is_right_angle_triangle(A, B, C)
% write("WQTGEFUYHBWEQJGKBKJSGHSDKJHFSKJLADHFSHFUWAHGIUOEWRHGUIREHGJKRHKGSJHKJRDFHGKJSDHGKJSDHKJGHDSFKJGHKJSDFHGKJDSFHGKJHDFSKGJHDSFKJGHDFSKJHGKJSDHGJKSDFH"), nl, 
% write("A - B - C"), nl,
% write(A), write(" - "), write(B), write(" - "), write(C), nl
		) -> 
			( asserta(maxil(fact)), fail ) 
				; 
			(( A is 1, B is 1, C is 1) -> (true); (fail)) ).
					
/* */
is_triangle(A, B, C) :-
	(C =< (A+B)),
	(B =< (A+C)),
	(A =< (C+B)).
/* */
is_right_angle_triangle(A, B, C) :- Cst2 is C*C, Ast2 is A*A, Bst2 is B*B,
	( (Cst2 is (Ast2+Bst2)) ; 
	  (Ast2 is (Cst2+Bst2)) ; 
	  (Bst2 is (Cst2+Ast2)) ).
/* */
perebor_do(1, 1) :- !.
perebor_do(N, Pereborka) :- (
	(Pereborka is N); (NewN is N-1, perebor_do(NewN, Pereborka))).
	
	