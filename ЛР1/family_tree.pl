/* task 1 */
man(artyom).
man(vladik).
man(ivan).
man(armen).
man(arsen).
man(zhenya).
man(sergey).
man(timur).
man(timothy).
man(peter).
man(kolya).
man(suren).
man(kirill).
man(danil).
man(andrey).
man(dima).

woman(ale).
woman(nadia).
woman(kate).
woman(vika).
woman(lisa).
woman(nika).
woman(pauline).
woman(asya).
woman(masha).
woman(lena).
woman(lera).
woman(nastya).
woman(anya).
woman(alyona).
woman(veronica).
woman(nina).

men() :- man(X), print(X), nl, fail.
women() :- woman(X), print(X), nl, fail.

/* parent(X, Y) where X is the parent of Y */
parent(artyom, vladik).
parent(ale, vladik).
parent(zhenya, sergey).
parent(zhenya, lisa).
parent(vika, sergey).
parent(vika, lisa).
parent(timothy, peter).
parent(timothy, kolya).
parent(asya, peter).
parent(asya, kolya).
parent(suren, masha).
parent(suren, anya).
parent(nastya, masha).
parent(nastya, anya).
parent(andrey, nina).
parent(veronica, nina).

parent(ivan, artyom).
parent(ivan, zhenya).
parent(kate, artyom).
parent(kate, zhenya).
parent(arsen, ale).
parent(arsen, vika).
parent(arsen, timothy).
parent(nika, ale).
parent(nika, vika).
parent(nika, timothy).
parent(timur, asya).
parent(timur, suren).
parent(pauline, asya).
parent(pauline, suren).
parent(kirill, nastya).
parent(kirill, andrey).
parent(lera, nastya).
parent(lera, andrey).
parent(dima, veronica).
parent(alyona, veronica).

parent(danil, kirill).
parent(lena, kirill).
parent(armen, kate).
parent(nadia, kate).

/* construct a predicate children(+X), which displays all children of X */
children(X) :- parent(X, Y), print(Y), nl, fail.

/* construct a predicate mother(?X, +Y), which determines whether X is the mother of Y  or tries to specify X as the mother of Y*/
mother(X, Y) :- woman(X), parent(X, Y), !.

/* construct a predicate mother(+X), which outputs the mother of X */
mother(X) :- mother(Y, X), print(Y).

/* construct a predicate brother(+X, +Y), which checks whether X is the brother of Y */
brother(X, Y) :- man(X), X\=Y, parent(T, X), parent(T, Y), !.

/* construct a predicate brothers(+X), which prints all brothers of X */
brothers(X) :- parent(T, X), !, parent(T, Y), man(Y), Y\=X, print(Y), nl, fail.

/* construct a predicate b_s(+X, +Y), which checks whether X and Y are siblings */
b_s(X, Y) :- parent(T, X), parent(T, Y), !, X\=Y.

/* construct a predicate b_s(+X), which displays all brothers or sisters of X */
b_s(X) :- parent(T, X), !, parent(T, Y), X\=Y, print(Y), nl, fail.

/* task 2 */
/* construct a predicate daughter(?X, +Y), which checks whether X is the daughter of Y  or tries to specify X as a daughter of Y*/
daughter(X, Y) :- woman(X), parent(Y, X), !.

/* construct a predicate daughter(+X), that prints the daughter of X. */
daughter(X) :- daughter(Y, X), print(Y), nl.

/* construct a predicate wife(?X, +Y) that tests whether X is the wife of Y or tries to specify X as a wife of Y */
wife(X, Y) :- woman(X), man(Y), parent(X, T), parent(Y, T), !.

/* construct a predicate wife(+X) that outputs the wife of X. */
wife(X) :- wife(Y, X), print(Y).

/* task 3 */
/* construct a predicate grand_pa(+X, +Y) that tests whether X is the grandfathert of Y. */
grand_pa(X, Y) :- man(X), parent(X, T), parent(T, Y), !.
/* another variant */
/* grand(+X, +Y) checks whether X is the grandparent of Y */
grand(X, Y) :- parent(X, T), parent(T, Y), !.
/* grand_pa(+X, +Y, ?Z) */
grand_pa(X, Y, Z) :- man(X), grand(X, Y).

/* construct a predicate grand_pas(+X) that prints all the grandfathers of X. */
grand_pas(X) :- parent(T, X), parent(Y, T), man(Y), print(Y), nl, fail.

/* construct the predicate grand_ma_and_son(+X, +Y), which checks whether X and Y are grandmother and grandson or grandson and grandmother */
grand_ma_and_son(X, Y) :-
    woman(X), man(Y), parent(X, T), parent(T, Y), !;
    man(X), woman(Y), parent(Y, T), parent(T, X), !.
/* another variant */
/* grand_ma_and_son(+X, +Y, ?Z) */
grand_ma_and_son(X, Y, Z) :-
    woman(X), man(Y), grand(X, Y), !;
    man(X), woman(Y), grand(Y, X), !.

/* construct a predicate nephew(+X, +Y) that checks whether X is the nephew of Y. */
nephew(X, Y) :- man(X), parent(T, X), T\=Y, parent(F, T), parent(F, Y), !.
/* another variant */
/* grand_all(-X, +Y) enumerates the concretization of X as the grandparent of Y */
grand_all(X, Y) :- parent(X, T), parent(T, Y).
/* nephew(+X, +Y, ?W) */
nephew(X, Y, W) :- man(X), not(parent(Y, X)), grand_all(Z, X), parent(Z, Y), !.

/* construct a predicate nephew(+X) that outputs all the nephews of X. */
nephew(X) :- parent(D, X), !, parent(D, G), X\=G, parent(G, Y), man(Y), print(Y), nl, fail.
