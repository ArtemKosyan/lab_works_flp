start_akinator :-
    retractall(answers(_, _)),
    book(Name_book),
    !,
    nl,
    write(" The book is "), write(Name_book), write(.), nl.

/* third question is crucial */
book("William Shakespeare The Tragedy of Hamlet") :-
    question("Is your book from foreign literature?"),
    question("Is your book written in verse form?"),
    question("Is there one main character in your book?").

book("William Shakespeare King Lear") :-
    question("Is your book from foreign literature?"),
    question("Is your book written in verse form?"),
    not(question("Is there one main character in your book?")).

book("Alexander Pushkin The Tale of the Dead Princess and the Seven Knights") :-
    not(question("Is your book from foreign literature?")),
    question("Is your book written in verse form?"),
    not(question("Is there one main character in your book?")).

/* fourth question is crucial */
book("Arthur Conan Doyle A Study in Scarlet") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    not(question("Is there one main character in your book?")),
    question("Do all the main characters stay alive at the end of the work?").

book("Alexander Pushkin The Tale of the Golden Cockerel") :-
    not(question("Is your book from foreign literature?")),
    question("Is your book written in verse form?"),
    question("Is there one main character in your book?"),
    not(question("Do all the main characters stay alive at the end of the work?")).

book("Mikhail Bulgakov The Master and Margarita") :-
    not(question("Is your book from foreign literature?")),
    not(question("Is your book written in verse form?")),
    not(question("Is there one main character in your book?")),
    question("Do all the main characters stay alive at the end of the work?").

book("Leo Tolstoy War and Peace") :-
    not(question("Is your book from foreign literature?")),
    not(question("Is your book written in verse form?")),
    not(question("Is there one main character in your book?")),
    not(question("Do all the main characters stay alive at the end of the work?")).

/* fifth question is crucial */
book("Stephen King The Institute") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    question("Are there children among the main characters of the work?").

book("Stephen King The Dead Zone") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    not(question("Do all the main characters stay alive at the end of the work?")),
    not(question("Are there children among the main characters of the work?")).

book("Stephen King Firestarter") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    not(question("Is there one main character in your book?")),
    not(question("Do all the main characters stay alive at the end of the work?")),
    question("Are there children among the main characters of the work?").

book("Bram Stoker Dracula") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    not(question("Is there one main character in your book?")),
    not(question("Do all the main characters stay alive at the end of the work?")),
    not(question("Are there children among the main characters of the work?")).

book("Alexander Pushkin The Tale of Tsar Saltan") :-
    not(question("Is your book from foreign literature?")),
    question("Is your book written in verse form?"),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    question("Are there children among the main characters of the work?").

/* sixth question is crucial */
book("Stephen King Pet Sematary") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    not(question("Do all the main characters stay alive at the end of the work?")),
    question("Are there children among the main characters of the work?"),
    not(question("Does the hero act of his own free will?")).

book("Stephen King Carrie") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    not(question("Do all the main characters stay alive at the end of the work?")),
    question("Are there children among the main characters of the work?"),
    question("Does the hero act of his own free will?").

book("Stephen King Misery") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    not(question("Does the hero act of his own free will?")).

book("Fedor Dostoyevsky Crime and Punishment") :-
    not(question("Is your book from foreign literature?")),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    question("Does the hero act of his own free will?").

book("Alexander Pushkin The Tale about a Fisherman and a Fish") :-
    not(question("Is your book from foreign literature?")),
    question("Is your book written in verse form?"),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    question("Does the hero act of his own free will?").

book("Alexander Pushkin The Tale of the Priest and of his workman Balda") :-
    not(question("Is your book from foreign literature?")),
    question("Is your book written in verse form?"),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    not(question("Does the hero act of his own free will?")).

/* specialized issues */
book("Stephen King The Eyes of the Dragon") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    question("Does the hero act of his own free will?"),
    not(question("Is your book in the science fiction genre?")).

book("Isaac Asimov Lucky Starr") :-
    question("Is your book from foreign literature?"),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    question("Does the hero act of his own free will?"),
    question("Is your book in the science fiction genre?").

book("Nikolai Vasilievich Gogol Dead Souls") :-
    not(question("Is your book from foreign literature?")),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    not(question("Does the hero act of his own free will?")),
    not(question("Is there a love plot in your book?")).

book("Kuprin The Garnet Bracelet") :-
    not(question("Is your book from foreign literature?")),
    not(question("Is your book written in verse form?")),
    question("Is there one main character in your book?"),
    question("Do all the main characters stay alive at the end of the work?"),
    not(question("Are there children among the main characters of the work?")),
    not(question("Does the hero act of his own free will?")),
    question("Is there a love plot in your book?").

question("Is your book from foreign literature?") :-
    query("Is your book from foreign literature?").

question("Is your book written in verse form?") :-
    query("Is your book written in verse form?").

question("Is there one main character in your book?") :-
    query("Is there one main character in your book?").

question("Do all the main characters stay alive at the end of the work?") :-
    query("Do all the main characters stay alive at the end of the work?").

question("Are there children among the main characters of the work?") :-
    query("Are there children among the main characters of the work?").

question("Does the hero act of his own free will?") :-
    query("Does the hero act of his own free will?").

question("Is your book in the science fiction genre?") :-
    query("Is your book in the science fiction genre?").

question("Is there a love plot in your book?") :-
    query("Is there a love plot in your book?").

query(Ask) :-
    (   (answers(Ask, Reply), !, ((Reply = 'y', true); (fail, !)) );
    (nl,
     write(Ask),
     write(' (y/n)? '),
     read(Answer),
     (   ( 'y' = Answer, Reply = 'y', ! );
         ( Reply = 'n')    ),
     assert(answers(Ask, Reply)))    ),
    Reply = y.

